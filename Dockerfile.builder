FROM golang:1.12-alpine as builder

# Get ca-certificates, zoneinfo and git
RUN apk --update add ca-certificates tzdata git curl

# Get golangci-lint
RUN curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.17.1

# All these steps will be cached
RUN mkdir /app
WORKDIR /app
COPY go.mod . 
COPY go.sum .

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download

# COPY the source code as the last step
COPY . .